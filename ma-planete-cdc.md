---
title: Portail "ma-planete.info"
subtitle: Cahier des charges - v0.6.5
release: v0.6.5 03/11/2023
author: G. Ponçon
documentclass: book
---

&nbsp;

_Le [dérèglement climatique](https://fr.wikipedia.org/wiki/R%C3%A9chauffement_climatique) est un sujet vital et heureusement très abordé. Mais on oublie trop souvent qu'il n'est que l'une des **[neuf limites planétaires](https://fr.wikipedia.org/wiki/Limites_plan%C3%A9taires)** répertoriées. Ces dernières constituent les critères indispensables à l'habitabilité de notre planète. Six d'entre elles viennent d'être franchies en quelques décennies, une situation préoccupante._

_Une initiative consacrée à une de ces limites peut avoir des effets de bord sur les autres. Pour innover et agir de manière appropriée, il est nécessaire de maîtriser les enjeux liés aux limites planétaires dans leur globalité : leurs interactions, leurs causes, leurs conséquences directes et indirectes, pour la biosphère, l'homme, la société, la terre. La [voiture électrique](https://bonpote.com/la-voiture-electrique-solution-ideale-pour-le-climat/), par exemple, est un sujet d'étude édifiant._

_Aujourd'hui, il est difficile de se former sur la [systémique](https://fr.wikipedia.org/wiki/Approche_syst%C3%A9mique) de notre planète sans y consacrer beaucoup de temps et d'énergie. Pourtant, il est fondamental pour l'avenir de nos enfants et du vivant que la vision du monde évolue en intégrant ces risques._

_**"[ma-planete.info](https://ma-planete.info)" est un portail d'information et de veille** destiné d'une part à présenter ces enjeux par une communication simple et abordable, d'autre part à relayer les travaux des vulgarisateurs et des scientifiques via des pages d'information, un moteur de recherche efficace, des métriques parlantes et une API OpenData._

\pagebreak

\centerline{SOMMAIRE}

---

\setupcombinedlist[content][list={chapter,section},alternative=c]
\placecontent

---

\pagebreak

# Objectifs

"[ma-planete.info](https://ma-planete.info)" est le portail dont nous avons besoin pour aborder et approfondir les mécanismes du "système terre", leurs évolutions et l'importance de leurs enjeux.

1. Vulgariser et accompagner
2. Veiller et informer

## 1. Vulgarisation et accompagnement[^pageo1]

[^pageo1]: Pages de niveau 1 et 2

- Exposer ce que sont les **limites[^limite] planétaires**, leurs interactions, causes et conséquences, comme point de départ.
- Aborder la notion de **risques systémiques**, nécessaire à l'élaboration de solutions pertinentes.
- **Accompagner** le visiteur à chaque étape de sa démarche&nbsp;:
  - découvrir et approfondir ses connaissances _(apprendre)_,
  - digérer les infos et redéfinir sa vision du monde _(accepter)_,
  - faire évoluer son quotidien pour lui donner du sens _(agir)_.

[^limite]: Pour simplifier l'écriture de ce document, on désignera par le mot "limite" le système (changement climatique, acidification des océans, etc.) lié à une limite planétaire.

### Une introduction attractive et abordable

Résumer par des mots simples des mécanismes denses, complexes et techniques n'est pas une tâche facile. Le "site vitrine" du portail devra **introduire ces thèmes de manière concise et claire** pour les visiteurs non initiés au jargon des experts. Les premières minutes de consultation étant décisives pour chaque visiteur, la formulation de l'information est importante.

## 2. Veille et information[^pageo2]

[^pageo2]: Pages de niveau 3 et plus

- Fournir des **données actualisées** provenant des travaux de nombreux experts[^personnes] qui travaillent sur ces sujets, sous forme de pages d'information, statistiques et flux OpenData.
- Proposer des **liens vers de nombreuses sources** relatives à ces sujets pour les approfondir.

[^personnes]: Scientifiques, vulgarisateurs, chercheurs, journalistes spécialisés, entrepreneurs...

### Neutralité et objectivité

"[ma-planete.info](https://ma-planete.info)" est un site qui introduit, analyse et relaie l'information. Il fournit un contenu dynamique issu de sources sérieuses et de faits réels, en rapport avec les limites planétaires. En ce sens, il n'est ni un site d'opinion, ni un outil de militantisme. Le site n'aura pas pour but de critiquer les informations, mais simplement de les exposer, de fournir des métriques et proposer des liens vers leurs sources.

\pagebreak

# Contenu du site vitrine

Le site vitrine[^vitrine] de "[ma-planete.info](https://ma-planete.info)" sera divisée en trois grandes parties :

- **"Apprendre"** pour comprendre l'état du monde.
- **"Accepter"** que l'avenir soit incertain.
- **"Agir"** pour la planète et le vivant.

Chaque partie introduit des thèmes, expose des métriques et relaie des informations. Une API[^api] OpenData sera proposée pour permettre une exploitation technique de l'ensemble de ces données.

[^vitrine]: Partie visible du portail, accessible à tous.

[^api]: Une "API" est une interface technique qui permet de fournir des fonctionnalités et des données à des applications externes, telles que le site vitrine et d'autres sites web ou applications. Elle est à la disposition des développeurs web.

![Plan du site vitrine](plan-portail.png){width=100%}

## Page d'accueil

Elle fera apparaître un diagramme dynamique et interactif des limites planétaires inspiré de celui qui fut proposé dans [le rapport de Rockström et al. en 2009](https://www.nature.com/articles/461472a) puis dans [les publications du "Stockholm Resilience Centre"](https://www.stockholmresilience.org/planetary-boundaries). (section I.1)

![](page-accueil.png){width=100%}

Pour chaque limite&nbsp;: son nom et une explication simple, sa progression, un lien vers une page consacrée au thème et en rollover...

- une **explication** succincte[^explic],
- son **influence** sur les autres limites,
- les **causes** principales de la progression,
- les **conséquences** pour l'humanité,
- les **ressources** principales liées au sujet.

[^explic]: Qui propose en quelques mots la définition et le rôle de la limite.

Sous le diagramme des limites, sera proposée une introduction aux principales causes et conséquences de ces dépassements (section I.2), avec des zooms sur les solutions actuelles et leurs limitations[^zoom].

[^zoom]: L'idée est d'interroger de manière claire et schématique les objectifs de la solution, le pour et le contre, sa pertinence d'un point de vue systémique et les sources d'information sur ce sujet. Par exemple, la solution "voiture électrique" n'émet pas là où elle roule mais pollue indirectement par l'électricité consommée et une extraction importante de minerais nécessaire à sa fabrication. D'un point de vue systémique, il faut aussi considérer les conséquences d'une solution coûteuse sur les inégalités sociales, le traitement de ces futurs déchets à moyen-long terme et de nombreux autres effets de bord. De nombreux axes d'analyse qui font l'objet de métriques (données collectées) et de travaux à mettre en avant.

Enfin, les sections II.1 et III.1 de la page d'accueil concerneront des introductions des parties _accepter_ et _agir_, qui introduisent l'art et la manière de réagir à ces informations.

## Partie I "Apprendre"

Cette étape cruciale est nécessaire pour donner aux deux autres tout leur sens. Elle doit permettre d'apprendre et comprendre ces enjeux sur des bases solides, vérifiées et accessibles. Et donner l'envie d'étudier ces sujets.

### Section I.1 : une page par limite planétaire

![Les [limites planétaires en 2023](https://www.stockholmresilience.org/research/research-news/2023-09-13-all-planetary-boundaries-mapped-out-for-the-first-time-six-of-nine-crossed.html)[^srclimit]](planetary-boundaries-2023-fr.png){width=100%}

[^srclimit]: Credit: "Azote for Stockholm Resilience Centre, based on analysis in Richardson et al 2023", image ([originale](https://stockholmuniversity.box.com/s/sr0nfknm95oydnnsm1zj0c526qzjn1vs)) sous licence [CC BY-NC-ND 3.0](https://creativecommons.org/licenses/by-nc-nd/3.0/deed.fr), Stockholm Resilience Centre, trad. FR par "Ma Planète.".

1. Changement climatique
2. Déclin de la biodiversité[^5]
3. Modification de l'usage des sols
4. Exploitation de l'eau douce
5. Perturbation des cycles de l'azote et du phosphore
6. Acidification des océans
7. Aérosols atmosphériques
8. Diminution de la couche d’ozone
9. Pollution chimique[^6]

[^5]: "Erosion" est remplacé par "Déclin" qui est plus parlant au premier abord.
[^6]: Introduction d'entités nouvelles dans la biosphère.

Pour chacune de ces limites, une page introductive présente leur signification et leurs enjeux avec des mots simples. Un ou plusieurs graphiques dynamiques présentent les métriques essentielles. Enfin, des liens seront proposés vers du contenu pour approfondir le sujet&nbsp;:

- Pages en rapport avec la limite.
- Liens avec les autres limites[^7].
- Sujets qui ont une influence sur la limite.
- Principales statistiques liées au sujet.
- Impacts sociaux (analyses, actualités).
- Podcasts, conférences et données externes.
- Livres et oeuvres liés.

[^7]: L'idée est de montrer pour chaque limite ses influences directes sur les autres. Par exemple, le changement climatique a des répercutions évidentes sur la biodiversité. Ces liens peuvent apparaître de manière visuelle sur le schéma (roll-over).

### Section I.2 : causes et conséquences

L'objectif de cette partie, qui introduit des thèmes adjacents aux limites planétaires, est de permettre une vision systémique des actions de l'homme et leurs conséquences sur la planète.

On organisera ces thèmes de cette manière&nbsp;:

- Les **causes** potentielles du dépassement des limites.  
_(industrie, transports, tech, etc.)_
- Les **conséquences** directes du dépassement.  
_(fonte des glaces, dérèglements de la bio-diversité, etc.)_
- Des **exemples** de la vie quotidienne.  
_(voiture, chauffage, alimentation, etc.)_
- Les **"solutions"** actuelles et leurs effets de bord.  
_(ENR, voiture électrique, hydrogène, etc.)_

Pour chacun de ces items, une introduction sur...

- leurs influences positives ou négatives&nbsp;;
- leurs impacts sociaux&nbsp;;
- leur pertinence...

...et des informations / liens vers des ressources qui traitent du sujet.

## Partie II "Accepter"

Cette partie, qui fait le lien entre l'acquisition des connaissances et l'action, proposera d'accompagner ceux qui veulent prendre le problème en main et ont besoin de "digérer" ces informations au préalable. Une étape introspective dont les objectifs peuvent se définir ainsi&nbsp;:

- adopter une vision du monde à impact positif pour la planète et le vivant&nbsp;;
- mener une réflexion sur le sens et la pertinence de ses actions en tant que citoyen&nbsp;;
- aborder l'éco-anxiété et les moyens d'y faire face.

Exemples de thématiques&nbsp;:

- Pourquoi l'humanité veut de la croissance&nbsp;?
- Quels nouveaux repères pour un monde juste et stable&nbsp;?
- Quel rôle constructif puis-je avoir en fonction de mon profil&nbsp;?
- Comment faire le deuil de la croissance infinie&nbsp;?
- Quels seront les métiers d'avenir&nbsp;?

Ces thématiques et leurs liens évolueront en fonction des ressources qui traitent de ces sujets.

> "Aujourd’hui la liberté de rouler en SUV en centre-ville est en train de tuer notre liberté de continuer à pouvoir vivre dans un monde habitable."
>
> _\hfill Aurélien Barrau_

## Partie III "Agir"

Cette partie exposera le panel de solutions[^3] existantes ou à envisager pour travailler avec d'autres sur l'avenir, classées en deux grandes catégories&nbsp;:

[^3]: Ce sujet portera sur de véritables actions citoyennes qui impliquent une évolution majeure de nos objectifs de vie. Et non de simple "petits gestes" pour la planète, importants mais insuffisants.

- **Agir pour la planète** abordera des actions utiles et pertinentes comme axes d'amélioration&nbsp;: dépollution, développement durable, inventions positives, low-techs, militantisme, sobriété, recherche...
- **Agir pour notre résilience**[^4] abordera l'art et la manière de s'adapter aux difficultés à venir. Parmi ces sujets&nbsp;: la résilience territoriale, l'autonomie et la survie, l'entre-aide...

[^4]: La "résilience" n'est pas forcément écologique, c'est ce qui la distingue de "l'action pour la planète".

Ces informations constitueront une base connaissance complète, proposant des réalisations inspirantes, des idées d'actions et des liens vers ces informations. La navigation sera travaillée pour être dynamique et associée à un moteur de recherche efficace. Des métriques seront proposées pour aider les entreprises à faire des choix pertinents.

## Fonctionnalités

"[ma-planete.info](https://ma-planete.info)" est un portail "dynamique". En d'autres termes, il propose de l'information sans cesse actualisée à partir de nombreuses sources différentes. A retrouver dans le [plan de réalisation](#plan-de-réalisation-et-chiffrage).

### Fonctions du site vitrine

- Outil d'édition du contenu ([Markdown](https://fr.wikipedia.org/wiki/Markdown) versionnée)
- Edition dynamique des ressources externes[^edre]
- FAQ, Glossaire, popups automatiques
- Moteur de recherche
- Newsletter, blog
- Rétro-liens et badges ma-planete[^badge]

[^edre]: Les ressources externes seront essentiellement affichées sous forme de "cards" avec titre, résumé, illustration, lien vers l'article / l'actualité / les données. Un lien interne pointe sur la page thématique dédiée et le lien externe vers la source.

[^badge]: Liens et petits logos clicables à afficher sur les sites partenaires.

### Fonctionnalités dynamiques

- API OpenData
- Affichage de données statistiques en temps réel

Ce travail consiste en la mise en place d'agents de récupération de données qui sont par la suite traitées afin d'être mises à disposition ou affichées sous forme de graphiques. Par exemple, pour avoir des visuels en temps réel sur l'évolution de la population mondiale, du taux de CO2 dans l'air, de l'utilisation des énergies, etc. La mise en place technique de l'API OpenData est abordée plus loin.

### Fonctionnalités additionnelles

- Aggrégateur d'actualités issues de flux externes[^flux]
- Annuaire de bonne adresses pour débattre et agir
- Avis de l'expert[^expert]
- Accès multi-utilisateur + workflow
- Shortlinks dynamiques[^shortlinks]
- Statistiques d'utilisation du site[^stats]

[^flux]: Flux RSS / Atom ou récupérés via des agents spécialisés.

[^expert]: Permettre à un expert sur le sujet d'exposer son avis en 1 ou 2 phrases, affiché sous forme d'encadré lié à la ressource externe (actualité, article, etc.).

[^shortlinks]: Des liens intermédiaires courts pour accéder à une information ou une ressource, passant par le portail. Par exemple, un lien _ma-planete.info/arthur-keller_ à utiliser à chaque fois que Arthur Keller est cité, redirigeant vers une ressource interne ou externe au choix et permettant le stockage de statistiques d'utilisation (comptage, sources, etc.).

[^stats]: Nombre de clics sur les liens, comportement des utilisateurs.

### Idées complémentaires

Ces réalisations seront envisagées pour faire grandir le projet&nbsp;:

- Commentaires sur l'actualité[^cact]
- Tests de connaissance et d'orientation
- Tester la viabilité systémique de mon projet écologique[^viapro]
- Liste des "fausses croyances"[^faussecroyance] & explications
- Programme politique d'un territoire résilient et écologique
- Jeu "escape game" pour sauver la planète[^escgame]
- Base de connaissances, mindmap d'argumentation, datamining[^knowledgebase]
- Outil de recherche / collaboration[^collab]
- Présentation de produits durables + affiliations
- Journal des changements + flux RSS
- Moteur d'entraînement IA sur ces données + assistant IA[^ia]
- Graph de dépendances pondérées entre les données unitaires
- Section "Les limites planétaires expliquées aux enfants"

[^cact]: Interface destiné aux intervenants "experts" permettant de réagir à une actualité&nbsp;: lien vers l'actu, contenu du commentaire et diffusion automatique sur blog, réseaux, etc.

[^viapro]: Bien souvent, un projet écologique a pour objectif de résoudre un problème ciblé (dans un "silo" donné). L'idée de cette démarche est, d'une part d'analyser les impacts systémiques à effets de bords sur d'autres paramètres, d'autre part de s'interroger sur la viabilité et la pertinence du projet d'un point de vue économique, social, mise en oeuvre à grande échelle et à long terme...

[^faussecroyance]: Un réchauffement de 2°C n'est pas grave, il suffit d'enlever un pull. Une base de données de "fausses croyances" avec une introduction, liens vers des ressources qui traitent du sujet.

[^escgame]: Et comprendre si j'ai tout bien compris...

[^knowledgebase]: Le principe est de noter des informations, faits, événements, etc.&nbsp;; de leur associer mots clés, catégories, liaisons et sources de manière à former une "mindmap". Des algorithmes de datamining permettent de visualiser ce qui lie deux informations, découvrir l'écosystème d'un fait sur plusieurs niveaux, etc.

[^collab]: Ce rôle serait en grande partie assuré par [Mattermost](https://mattermost.com/) : plateforme pour collaborer et prendre des notes sur la base des fonctionnalités en place. L'idée est de pouvoir travailler à plusieurs sur l'acquisition des connaissances liées à la compréhension des causes et des conséquences du dépassement des limites planétaires, les fonctionnalités du site, etc.

[^ia]: Les progrès réalisés sur l'intelligence artificielle permettront bientôt de pouvoir mettre en place un assistant IA local avec un moteur d'entraînement (machine learning) dédié aux données du portail. Cela permettra aux visiteurs et aux experts de gagner beaucoup de temps en recherches et d'assister la rédactions sur des sujets liés à ceux abordés par le portail. Des initiatives telles que [llama.cpp](https://github.com/ggerganov/llama.cpp) permettent la mise en place d'un tel assistant IA. Cependant il y a deux réflexions à avoir pour la mise en place de ce module. La première est qu'il faut beaucoup de données d'entraînement pour que l'assistant puisse répondre de manière pertinente. La deuxième est éthique&nbsp;: l'entraînement IA et l'assistant consommment beaucoup d'énergie, même si des progrès sont sans cesse réalisés pour que les IAs soient moins gourmandes... avec les effets rebonds que nous connaissons.

## Ergonomie

L'ergonomie générale se voudra **simple** et sans fioriture, avec un minimum de graphisme et des codes visuels strictement nécessaires à la lisibilité du contenu et de la structure du portail.

Un **[code couleur](https://www.macapflag.com/blog/couleurs-signification/)**, choisi pour sa signification intuitive, sera associé à chaque partie&nbsp;:

- **Bleu pour "apprendre"** (fiabilité, clarté, vérité...)
- **Vert pour "accepter"** (optimise, espérance, nature...)
- **Orange pour "agir"** (enthousiasme, énergie, action...)

Des **pictogrammes** seront associés aux types de contenus proposés&nbsp;: information, lien, idée, source / auteur, etc. Ils seront utilisés pour des liens, encadrés et paragraphes.

Enfin, un **indicateur de temps** sera affiché en tête des pages d'information et des médias, pour estimer en un coup d'oeil le temps nécessaire à prévoir pour lire, écouter ou visionner le sujet.

\pagebreak

# Plan de réalisation et chiffrage

La réalisation technique se fera en parallèle de l'activité de recherche et de rédaction tout au long de la vie du projet. Les différentes étapes de réalisation pourront être abordées au fur et à mesure que le projet en obtiendra les moyens. L'ordre de réalisation, le contenu de chaque étape et son chiffrage peuvent évoluer.

## 0. spécifications, partenariat & crowdfunding (120jh)

Une **étape préliminaire** de lancement du projet&nbsp;:

- Cahier des charges
- Charte graphique initiale
- Plaquette d'information
- Recherche de partenaires
- Page de crowdfunding + campagne

Pour cette étape, des outils de communication et de gestion de projets sont mis en ligne en libre accès.

## 1. Version minimale (90jh)

Un **frontend** contenant les pages principales de l'application&nbsp;:

- Page d'accueil[^pacc]
- Pages thématiques de 1er niveau[^them]
- Une page d'accueil par section
- Newsletter + Blog[^newsblog]

[^pacc]: Comprenant le gabarit et le contenu, dont le graphs interactif des limites planétaires.

[^them]: Une page par limite planétaire et par sujet adjacent, une page par thème pour chaque partie du site vitrine.

[^newsblog]: Fonctionnalités séparées de l'application dans un premier temps (wordpress dédié).

Résumé des travaux à effectuer&nbsp;:

- Chaîne CI/CD
- Architecture applicative du frontend
- Maquette graphique + intégration
- Hébergement initial
- Rédaction des pages principales

## 2. Gestion dynamique du contenu (60-120jh)

Mise en place d'une application **backend** permettant le développement de fonctionnalités dynamiques&nbsp;:

- Outil d'édition [Markdown](https://fr.wikipedia.org/wiki/Markdown)
- Moteur de recherche[^search]
- Versionning & gestion de l'historique
- Outil d'enregistrement / édition de ressources externes[^rext]
- Rédaction du contenu dynamique initial
- Glossaire avec création dynamique de liens[^glosslink]
- Foire aux questions (FAQ)
- Newsletter, blog, changelog

[^search]: Une implémentation très efficace, tolérante aux erreurs, permettant la génération de liens bookmarkables.

[^rext]: Les ressources externes peuvent être des articles, des billets, des sites ou chaînes thématiques avec ou sans flux d'actualité. Chaque ressource aura un titre, une description, des tags et d'autres contenus associés pour générer des fiches et des liens sur le site vitrine.

[^glosslink]: La clé "risques systémiques" dans une phrase sera "décorée" d'une popup précisant la signification de cette expression.

## 3. Aggrégateur et annuaire (60jh)

Des fonctionnalités qui permettront la publication automatique de fiches provenant de sources externes.

- Mise en place d'un aggrégateur de flux RSS
- Page d'actualité avec recherche / filtrage
- Mise en place d'un annuaire dynamique
- Liens automatiques vers les partenaires référencés[^autolink]
- Génération d'un billet d'actualité hebdomadaire[^billetauto]

[^autolink]: Ecrire "Low-Tech Lab" dans un texte génèrera un lien automatique vers la fiche du partenaire. Eventuellement utiliser un tag de type `{{low-tech-lab}}`.

[^billetauto]: Billet d'actualité généré, éditable, à publier sur le blog et les réseaux sociaux.

## 4. API OpenData (120-720jh)

Cette fonctionnalité consiste à récupérer automatiquement des données[^rad], les traiter (filtrer, simplifier, croiser, comparer) et les diffuser sous forme de graphs ou de flux structuré (JSON). Elle implique le développement de robots qui travaillent en tâche de fond.

[^rad]: Via des robots et des APIs opendata publiques ou de partenaires.

Il existe de nombreuses sources de données exploitables relatives aux limites planétaires, à leurs causes et leurs conséquences. Récupérer, traiter, croiser ces données, détecter les actions / réactions, diffuser des visuels graphiques permettant de les comparer, permettrait de mieux détecter et comprendre les liens qui existent entre ces métriques.

- Robots de provisionnement (récupération des données source)
- Traitement temps réel des données dans un format normalisé
- Création automatique de stats, graphs et rapports
- Mise à disposition via une API OpenData publique
- Estimations calculées des limites planétaires[^calclimit]

[^calclimit]: Avec suffisemment de données sources, un traitement et des pondérations appropriées, il serait possible d'estimer automatiquement les niveaux de dépassement des limites planétaires et de fournir des métriques pour chacunes d'elles. Par exemple, à partir des mesures de CO2 dans l'atmosphère et considérant le niveau acceptable, fournir la métrique sur l'année en cours et les années passées.

Cette fonctionnalité nécessite une architecture scalable et distribuée afin d'absorber la charge des agents de provisionnement et de traitement. Les outils à utiliser pour cette application sont décrits plus loin dans la section "caractéristiques techniques".

## 5. Fonctionnalités tierces

D'autres fonctionnalités pourront être implémentées tout au long de la vie du projet. Un certain nombre d'idées sont proposées dans ce document dans le paragraphe "[autres idées en vrac](#autres-idées-en-vrac)".

## 6. Maintenance et gestion du site

Elle concernera la mise à jour du contenu au fur et à mesure, la mise à jour technique[^majt], la maintenance de l'hébergement. En fonction de l'avancée du projet, cette tâche peux prendre 1 à 5 jh par semaine, voir nécessiter un travail d'équipe à plein temps.

[^majt]: migration des technologies au fur et à mesure qu'elles évoluent, corrections de sécurité et d'affichage au fur et à mesure que les supports évoluent.

Des travaux de **veille et recherche** continus seront nécessaires pour proposer un contenu actualisé. Cette tâche à réaliser en continu sera relativement chronophage et consistera entre autre à&nbsp;:

- Écouter et intégrer des conférences, podcasts, interviews.
- Lire et intégrer des livres[^livres] sur ces sujets.
- Lire et intégrer des rapports et articles scientifiques.
- Écouter et intégrer l'actualité.
- Résumer, vulgariser, lier ces informations.
- Produire des actualités, vidéos sur ces sujets.

[^livres]: Proposer aux auteurs l'envoi de leurs livres contre de la communication.

\pagebreak

# Qui sommes-nous ?

"ma-planete.info" a besoin d'une équipe active pour&nbsp;:

- Définir et lancer le projet.
- Communiquer, animer, trouver partenaires et des moyens.
- Travailler sur la partie éditoriale.
- Travailler sur le graphisme, la technique, la maintenance.
- Animer l'équipe.

Les partenaires du projets seront essentiels pour&nbsp;:

- Fournir des moyens (financements, hébergement, outils...).
- Booster la visibilité du site vitrine avec des liens et rétro-liens.
- Participer au contenu, le faire valider par des experts.
- Soutenir la démarche (journalistes, relais d'information...).

## Membres

Section à compléter.

## Partenaires

Section à compléter.

## Outils de collaboration

L'application opensource **[Mattermost](https://mattermost.com/)** (semblable à Slack) pourra être utilisée pour échanger et travailler sur le portail. Les autres outils techniques devraient faire partie intégrante des développements.

\pagebreak

# Financement

Il assurera le développement du portail, son hébergement, sa maintenance technique et son animation. Une période de lancement sera mise en place avec des recherches de partenaires et l'organisation d'un crowdfunding, suivi d'un financement régulier par abonnement.

## Estimations

### Lancement du projet, assuré à partir de 7500€

Au minimum :

- Hébergement[^heberg] et outils de développement[^outils] : **1000€ / 2 ans**
- Développement : **6000€** pour la version initiale du site vitrine
- Communication[^com] : **500€** au total pour 1 an

[^heberg]: Hébergement sur serveurs VPS ou dédiés dans un premier temps, puis cloud K8S dans un deuxième temps (non compris dans le montant de lancement), chez un hébergeur qui produit des efforts pour limiter la consommation d'énergie. L'achat des noms de domaines (~25€/an/ndd) et les éventuels coûts d'infogérance sont compris dans ce montant.

[^outils]: Logiciels de développement professionnels garantissant une bonne productivité (PHPStorm), template graphique, etc.

[^com]: Campagnes initiales de promotion sur les réseaux sociaux, outil de publication multi-réseaux, abonnement premium LinkedIn, etc.

### Lancement complet, à partir de 25000€

- **4000€** : charte graphique réalisée par un professionnel
- **6000€ - 12000€** : gestion dynamique du contenu
- **4000€ - 8000€** : aggrégateur de contenu externe et annuaire
- **10000€ - 30000€** : production de métriques + API OpenData
- **2000€ / mois** : développement, maintenance, animation

### Coûts annuels, assurés à partir de 6500€ / an

Objectif au minima : assurer les développements et la maintenance technique, l'animation et le contenu du site.

- **500€ / mois min.** pour un travail à temps partiel&nbsp;: maintenance technique essentielle, modération et communication minimales, permettant au projet d'exister dans la durée.
- **500€ / an** d'hébergement, noms de domaines, outils de développement.

Cependant, **un objectif minimal de 25000€ de budget par an** est estimé pour permettre au portail de prospérer sur la durée avec ses fonctionnalités essentielles&nbsp;:

- **2000€ / mois**, taxes incluses, pour un travail à temps plein, 7j/7, 365j/365&nbsp;: maintenance et évolutions techniques, animation et communication, développements continus, pour permettre au projet de se renforcer d'année en année.
- **1000€ / an** d'hébergement (VPS), noms de domaines, outils de développement.

Pour aller plus loin :

- **2000€ à 8000€ / mois** pour un hébergement sur cloud décentralisé "GreenIT"[^cloudd]
- **2000€ / mois** par salarié à temps plein
- **0€ à xxxx€ / mois** de communication et publicité
- **0€ à xxxx€ / mois** pour financer des projets partenaires

[^cloudd]: L'hébergement cloud décentralisé permet de proposer des instances locales pour limiter l'utilisation du réseau internet, répartir la charge et n'utiliser que les ressources matérielles nécessaires à la charge instantanée. Le coût d'un hébergement cloud est souvent très variable, car calculé à l'utilisation.

## Lancement (crowdfunding)

L'opération de crowdfunding sera lancée sur Zeste. Le planning proposé actuellement peut faire l'objet d'importantes évolutions en fonction des opportunités et de la tournure que prends le projet.

### Préparation (3-6 mois)

- Cahier des charges initial (ce document).
- Texte de présentation + plaquette.
- Charte graphique initiale.
- Vidéo de promotion.
- Personnes à contacter en priorité :
  - Equipe de projets[^equipe], >= 30 profils
  - Partenaires initiaux (financement, promotion, coworking)
- Personnes à contacter avant l'opération :
  - Partenaires & soutiens[^partsout], >= 200 profils
  - Scientifiques & experts

[^equipe]: Personnes prêtes à participer au projet et faire partie de l'équipe.
[^partsout]: Partenaires (personnes physiques ou morales) ok pour faire du buzz pendant le crowdfunding et soutenir le projet dans la durée.

### Campagne : (3-4 mois)

A organiser avec Zeste et les partenaires.

### Contreparties envisagées

- Achat de voix permettant de voter pour des fonctionnalités
- Livres / magazines électroniques de partenaires
- Accès aux versions beta des fonctionnalités
- Adresse prenom.nom@ma-planete.info avec redirection[^email]
- Affichage dans la liste des partenaires / soutiens[^affsout]

[^email]: Redirection seule vers une adresse valide (pas de compte e-mail). Soumis à conditions sur l'usage qui en est fait.

[^affsout]: Trié à la hauteur des montants consacrés.

### Développements

Les développements initiaux prendront environ 6 mois à temps plein, 2 ans pour un portail complet, par un bon développeur.

## Autres sources de financement

- Subventions
- Tipeee / Patreon

\pagebreak

# Visibilité

La visibilité du site sera assurée par&nbsp;:

- Actualités sur les réseaux sociaux & journaux.
- Référencements par rétro-liens et liens courts.
- Vidéos Youtube sur chaîne dédiée.
- Mentions des partenaires et visiteurs.

Une stratégie SEO sera mise en place avec&nbsp;:

- Un référencement optimisé pour chaque page.
- Des pages d'atterissage[^pageatt] pour les notions essentielles.

[^pageatt]: Le principe est de fournir des liens courts explicites pour les notions essentielles, afin que les utilisateurs du portail puissent les utiliser facilement dans leurs publications. Par exemple&nbsp;: ma-planete.info/rechauffement-climatique, ma-planete.info/azote, etc.

\pagebreak

# Caractéristiques techniques

Comme toutes les applications web, ma-planete.info va consommer du CPU et du réseau, participant à "transformer la nature en déchets". Les choix techniques doivent être pensés pour limiter cet impact au maximum, afin de maintenir une cohérence avec l'esprit du projet. On préviligiera&nbsp;:

- Une conception bas-carbonne[^cbc].
- Les logiciels et technologies open-source[^opensource].
- Un hébergement à faible consommation.

[^cbc]: Algorithmes optimisés, cache efficace, faible sollicitation du réseau, etc.

[^opensource]: L'opensource privilégie le partage du savoir, l'accessibilité sans discrimination, une juste répartition des richesses, la mise en [commun](https://lescommuns.org/).

## Frontend et Backend

Pour le **frontend**, une application simple, responsive[^responsive] et multi-support[^ms], optimisée pour limiter les traitements CPU. Un design sobre avec des illustrations vectorielles[^vect] de préférence et une taille globale limitée.

[^responsive]: Qui s'adapte dynamiquement à n'importe quelle taille d'écran.
[^ms]: Smartphone, tablette ou ordinateur de bureau, même anciens.
[^vect]: Les images vectorielles prennent moins de place et de temps de calcul que les bitmaps. Elles peuvent être redimentionnées sans limite.

Les **flux de communication** (JSON) entre backend et frontend (client et serveur) seront réduits au strict minimum afin de limiter les sollicitations réseau et maintenir une bonne fluidité, quelle que soit la connexion utilisée.

Enfin, le **backend** (API [REST](https://fr.wikipedia.org/wiki/Representational_state_transfer)) sera développé pour limiter et factoriser les sollicitations CPU et mémoire. Une [architecture trois tiers](https://fr.wikipedia.org/wiki/Architecture_trois_tiers) pensée pour être fractionnée en [micro-services](https://fr.wikipedia.org/wiki/Microservices), [full-scalable](https://fr.wikipedia.org/wiki/Extensibilit%C3%A9)[^scal], permettra en outre un déploiement sur un cluster Kubernetes [K3S](https://k3s.io/)[^k3s] capable de fournir de bonnes performances avec des petits serveurs[^serveurs].

[^scal]: La scalabilité est la capacité à supporter n'importe quelle variation de charge. Par exemple, passer de 10 à 10000 visiteurs d'un coup, suite à une campagne de visibilité.
[^k3s]: K3S est une implémentation légère de Kubernetes, permettant un déploiement sur une ferme de serveurs (cloud). La scalabilité est obtenue en développant une architecture logicielle capable de supporter les variations de charge par de simple ajouts ou retrait de "noeuds" (serveurs).
[^serveurs]: L'idée est de pouvoir augmenter la durée de vie des vieux serveurs qui travailleront en parallèle pour fournir les services en temps et en heure.

Technologies envisagées :

- **Frontend** : Tailwind + Vue + Chart.js + [SVG interactif](https://www.petercollingridge.co.uk/tutorials/svg/interactive/interactive-map/)
- **Backend** : PHP8 + Phalcon[^comp]
- **Stockage** : Redis + MariaDB + ElasticSearch ou équivalent

Pour travailler :

- **Kanboard** : outil de gestion de projet
- **Mattermost** : outil de communication
- **Forgejo / codeberg** : alternative libre à github

[^comp]: Composants indépendants ou issus d'autres frameworks si nécessaire. La régle étant d'avoir le moins de dépendances possible.

## Architecture du portail OpenData

![Architecture du portail OpenData](archi-opendata.png){width=100%}

Le flux des données suit le schéma général suivant :

- Les données externes sont récupérées puis lues par des services de provisionnement.
- Ces données sont ensuite transformées pour être normalisées. Cette étape permet de fournir une structure de données unique à chaque type de données (co2, géographies, etc.) pour faciliter les traitements ultérieurs et s'abstraire des formats disparates des sources.
- Les données normalisées sont ensuite stockées, analysées et mises à disposition du portail et de services externes via une API.

L'architecture technique est composée d'un ou plusieurs clusters de provisionnement et de normalisation, ainsi que d'un cluster principal de stockage des données normalisées et d'accès à l'API.

Les clusters locaux peuvent être déployés dans des zones géographiques différentes et sont entièrement scalables grâce à leur architecture basée sur K3S. Le cluster principal est unique et lui aussi 100% scalable, s'adaptant à la charge des services externes.

## Autres spécifications

Quelques caractéristiques dont il faudra tenir compte pour la mise en place de la solution technique.

### Architecture multilingue et multi-territoire

Des instances interconnectées doivent pouvoir être déployées sur plusieurs hébergements. Les mises à jour techniques concerneront l'ensemble des instances, tandis que le contenu éditorial et celui des bases de données locales seront spécifiques à chaque instance.

### Priorité aux performances

Le passage d'une page à une autre ne devra pas exceder 50ms en local et 200ms sur un réseau lent. Le rechargement de pages complètes est interdit. Le frontend sera le plus léger possible, le backend optimisé pour les performances et les économies de ressources.

### Monitoring

Mise en place d'agents de surveillance (monitoring technique) qui s'assurent que le portail est disponible en ligne et les services fonctionnels.

### Tests de non-régression

Tests unitaires pour les fonctionnalités critiques et scénarios anti-régressions à jouer sur environnements de développement et de tests. Le tout intégré à la chaîne d'intégration continue.

## Hébergement

- Environnement Docker + K3S ou K8S.
- Hébergement monolitique dans un premier temps (serveur dédié ou VPS), suivi d'un cloud / cluster.
- Provider (cluster principal) situé en France (local).
- Noms de domaine ma-planete.info et maplanete.info.

## Développement et qualité du code

- Une chaîne d'intégration / déploiement continu ([CI/CD](https://fr.wikipedia.org/wiki/CI/CD)) sera mise en place avec des outils libres.
- Chaque fonctionnalité sera développée dans une nouvelle branche GIT et mergée sur le tronc commun en fin de cycle agile.
- La branche principale contient du code stable et vérifié.
- Des stratégies de détection d'erreurs et d'amélioration de code seront mises en place.

\pagebreak

# Contact

- Par e-mail : <contact@ma-planete.info>
