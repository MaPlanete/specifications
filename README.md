# Spécifications du portail ma-planete.info

_"[ma-planete.info](https://ma-planete.info)" est un **portail d'information sur les limites planétaires**, destiné d'une part à introduire ces enjeux par une communication simple et abordable, d'autre part à relayer les travaux des vulgarisateurs et des scientifiques via des pages d'information, un moteur de recherche efficace et des métriques exposées sous forme de statistiques ou d'une API OpenData._

## Documents de spécification

- `ma-planete-cdc.md` : cahier des charges

## Edition et création des documents

Les documents sont au format Markdown, il existe de nombreuses solutions pour générer un rendu PDF.

Sous Linux, installer les binaires `gponcon/bin` et l'exécutable `pandoc`. La création est gérée par un makefile&nbsp;:

- `make` ou `make desktop` : document pdf A4 + visualisation
- `make tablet` : document A6 pour tablettes + visualisation
- `make all` :  création tous les documents
- `make clean` : supprime les documents générés
