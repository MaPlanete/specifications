.PHONY: desktop tablet cdc edit all clean

# Phony gen : cat Makefile | grep -e '^[a-z][a-z-]*:[a-z -]*$' | sed 's/:.*$//' | tr "\n" ' '

MD2PDF=md2pdf
MD2TABLET=md2tablet
FILECDC=ma-planete-cdc.md

desktop:
	$(MD2PDF) $(FILECDC) && xdg-open $(FILECDC).pdf

tablet:
	$(MD2TABLET) $(FILECDC) && xdg-open $(FILECDC).tablet.pdf

cdc:
	$(MD2PDF) $(FILECDC)
	$(MD2TABLET) $(FILECDC)

edit:
	xdg-open $(FILECDC)

all: cdc

clean:
	rm -f *.md.pdf *.md.tablet.pdf 2> /dev/null
